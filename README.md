# IMG interview test solution

This is a pretty barebones solution, the logic is tested in the specs, you can run them with:
```mvn verify```

Apologies for the lack of documentation, but this codebase is small enough to figure out without
any
