package com.laithalissa.img

import org.scalatest.FlatSpec

class StreamParsingSpec extends FlatSpec {
  behavior of "Reading file1"
  private final val resourcesDir = "src/test/resources/"

  it should "read sample1 successfully" in {
    new FileParser().getEvents(s"$resourcesDir/sample1.txt").toList
  }

  it should "read sample2 successfully" in {
    new FileParser().getEvents(s"$resourcesDir/sample2.txt").toList
  }
}
