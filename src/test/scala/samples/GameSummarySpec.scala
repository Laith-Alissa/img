package com.laithalissa.img

import org.scalatest.FlatSpec

class GameSummarySpec extends FlatSpec {
  behavior of "GameSummary"

  it should "Successfully initialise with no events" in {
    GameSummary(Nil)
  }

  it should "Allow you to add valid events" in {
    var game = GameSummary(Nil) // Don't try this at home
    val event = Event(
      pointsScored = 2,
      scorer       = 0,
      teamTwoScore = 0,
      teamOneScore = 2,
      elapsedTime  = 15
    )
    game = game.addEvent(event)
    game = game.addEvent(event.copy(scorer = 0, teamOneScore = 4, elapsedTime = 30))

    assert(game.getLastEvent.get.teamOneScore == 4)
  }

  it should "Correct an invalid team total" in {
    var game = GameSummary(Nil)
    val event = Event(
      pointsScored = 2,
      scorer       = 0,
      teamTwoScore = 0,
      teamOneScore = 2,
      elapsedTime  = 15
    )
    game = game.addEvent(event)
    game = game.addEvent(event.copy(scorer = 0, teamOneScore = 100, elapsedTime = 30))
    assert(game.getLastEvent.get.teamOneScore == 4)
  }
}
