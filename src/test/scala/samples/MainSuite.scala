package com.laithalissa.img

import org.scalatest.Suites

class MainSuite extends Suites(new GameSummarySpec, new EventSpec, new StreamParsingSpec)
