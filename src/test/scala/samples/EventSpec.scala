package com.laithalissa.img

import org.scalatest.FlatSpec

class EventSpec extends FlatSpec {
  behavior of "An Event"

  it should "Parse a binary event correctly" in {
    // First example
    val event1 = Event(0x781002)
    assert(event1.pointsScored == 2)
    assert(event1.scorer       == 0)
    assert(event1.teamTwoScore == 0)
    assert(event1.teamOneScore == 2)
    assert(event1.elapsedTime  == 15)

    // Second example
    val event2 = Event(0xf0101f)
    assert(event2.pointsScored == 3)
    assert(event2.scorer       == 1)
    assert(event2.teamTwoScore == 3)
    assert(event2.teamOneScore == 2)
    assert(event2.elapsedTime  == 30)
  }
}
