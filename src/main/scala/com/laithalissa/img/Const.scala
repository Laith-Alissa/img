package com.laithalissa.img

object Const {
  final val HEX_PREFIX = "0x"
  final val MSB_MASK = 0x7FFFFFFF

  // & With this mask to get the score of an event
  final val SCORE_BITS = 2
  final val SCORE_SHIFT = 0

  // & + shift get the team who scored
  final val SCORER_BITS = 1
  final val SCORER_SHIFT = SCORE_BITS

  // Team 2 points
  final val TEAM2_SCORE_BITS = 8
  final val TEAM2_SHIFT = SCORER_SHIFT + SCORER_BITS

  // Team 1 points
  final val TEAM1_SCORE_BITS = 8
  final val TEAM1_SHIFT = TEAM2_SHIFT + TEAM2_SCORE_BITS

  // Match time
  final val ELAPSED_TIME_BITS = 12
  final val ELAPSED_TIME_SHIFT = TEAM1_SHIFT + TEAM1_SCORE_BITS
}
