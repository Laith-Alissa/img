package com.laithalissa.img

import com.laithalissa.img.Util.{LSBMask => mask}

case class Event(
  pointsScored: Int,
  scorer: Int,
  teamTwoScore: Int,
  teamOneScore: Int,
  elapsedTime: Int
) extends Ordered[Event] {
  def compare(that: Event): Int = this.elapsedTime compare that.elapsedTime
}

object Event {
  def apply(event: Int): Event = new Event(
    pointsScored = event                              & mask(Const.SCORE_BITS),
    scorer       = event >>> Const.SCORER_SHIFT       & mask(Const.SCORER_BITS),
    teamTwoScore = event >>> Const.TEAM2_SHIFT        & mask(Const.TEAM2_SCORE_BITS),
    teamOneScore = event >>> Const.TEAM1_SHIFT        & mask(Const.TEAM1_SCORE_BITS),
    elapsedTime  = event >>> Const.ELAPSED_TIME_SHIFT & mask(Const.ELAPSED_TIME_BITS)
  )
}
