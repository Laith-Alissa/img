package com.laithalissa.img

case class GameSummary(val events: Seq[Event]) {

  // TODO: We could get events out of order, we would need to find the right "previous event"
  def addEvent(newEvent: Event): GameSummary = {
    getLastEvent() map {
      lastEvent: Event => {
        val isValid = newEvent.scorer match {
          case 0 => lastEvent.teamOneScore + newEvent.pointsScored == newEvent.teamOneScore
          case 1 => lastEvent.teamTwoScore + newEvent.pointsScored == newEvent.teamTwoScore
        }
        if (isValid) {
          this.copy(events = (events :+ newEvent).sorted)
        } else {
          val newTeamOneScore = if (newEvent.scorer == 0) {
            lastEvent.teamOneScore + newEvent.pointsScored
          } else lastEvent.teamOneScore

          val newTeamTwoScore = if (newEvent.scorer == 1) {
            lastEvent.teamTwoScore + newEvent.pointsScored
          } else lastEvent.teamTwoScore

          this.copy(events = (events :+ lastEvent.copy(
            pointsScored = newEvent.pointsScored,
            scorer = newEvent.scorer,
            teamOneScore = newTeamOneScore,
            teamTwoScore = newTeamOneScore
          )).sorted)
        }
      }
    } getOrElse {
      this.copy(Seq(newEvent))
    }
  }
  def getLastEvent(): Option[Event] = events.lastOption
  def getLastEvents(number: Int): Seq[Event] = events.takeRight(number)
  def getAllEvents(): Seq[Event] = events
}
