package com.laithalissa.img

import scala.io.Source

trait FileParsing {
  def readFile(path: String): Iterator[String] = Source.fromFile(path).getLines

  def processLine(line: String): Option[Event] = {

    val l = line.stripPrefix(Const.HEX_PREFIX)
    if (l.isEmpty || BigInt(l, 16) >> 32 != 0) {
      None
    } else {
      Some(Event(Integer.parseInt(l, 16)))
    }
  }

  def getEvents(filePath: String): Iterator[Event] = {
    readFile(filePath) flatMap processLine
  }
}

class FileParser() extends FileParsing
