package com.laithalissa.img

object Util {
  def LSBMask(numberOfBits: Int): Int = (1 << numberOfBits) - 1
}
